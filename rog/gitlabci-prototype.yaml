workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_PIPELINE_SOURCE == "push" && ($CI_MERGE_REQUEST_PROJECT_PATH =~ /^redhat.centos-stream/ || $CI_PROJECT_PATH =~ /^redhat.centos-stream/)'

# Default image for executing the pipeline. Using CKI python image for lazyness
# for now.
image: images.paas.redhat.com/osci/python:latest

stages:
  - pre_build
  - start_build
  - build
  - generate_tests
  - test
  - get_results
  - merge_check
  - promote


.with_artifacts:
  artifacts:
    when: always
    paths:
      - test_runs.yml  # Generated dynamic pipeline
      - metadata.env   # Information to pass along as needed, e.g. build ID and link
    reports:
      junit:
        - test_report.xml  # Test results to display
      dotenv:
        - metadata.env  # Information to pass along as needed, e.g. build ID and link
    expire_in: 6 months

# Template for handling retries for weird GitLab failures. This has nothing to do
# with retries for test execution issues.
.with_retries:
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
      - unknown_failure
      - api_failure

check-gitbz:
  image: registry.gitlab.com/redhat/centos-stream/ci-cd/dist-git-gating-tests/check_bz:latest
  tags:
    - redhat
    - gitbz
  stage: pre_build
  script:
    - |
      MR_COMMENT="To start testing and gating of this MR, go the pipeline linked at the beginning of the page and run the <b>start</b> job.<br/><img src=\"https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines/-/raw/main/rog/start_pipeline.png\"/><br/>Do not forget to verify this MR carefully in case it comes from a non-RH employee.<br/>The downstream target is set with MR labels as follows: <ul><li><code>target::latest</code>: latest y-stream (set by default)</li><li><code>target::zstream</code>: z-stream</li><li><code>target::exception</code>: exception</li></ul><br/>You can use the <code>/label</code> command in a comment to set label.<br/>Do not forget to restart the pipeline after updating labels."
    - exit_code=0
    - /usr/bin/check_gitbz || exit_code=$?
    - |
      if [ $exit_code -eq 77 ]; then
        MR_COMMENT=$(echo $MR_COMMENT | sed 's/^/<h4>NOTICE: This change is being allowed without approval during the active development phase.<\/h4><\/br>/')
        echo "MR_COMMENT=$MR_COMMENT" >> check-gitbz.env
        exit 0
      fi
    - |
      if [ $exit_code -gt 0 ]; then
        MR_COMMENT=$(echo $MR_COMMENT | sed 's/^/<h4>WARNING: Either no related issue was found or the issues that were found were not approved for blocker\/exception. Merge not allowed.<\/h4><\/br>/')
        echo "MR_COMMENT=$MR_COMMENT" >> check-gitbz.env
        exit 0
      fi
    - echo "MR_COMMENT=$MR_COMMENT" >> check-gitbz.env
  artifacts:
    paths:
      - "debug.log"
    reports:
      dotenv: check-gitbz.env
    when: always
    expire_in: 1 week
  rules:
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'

merge-check-gitbz:
  image: registry.gitlab.com/redhat/centos-stream/ci-cd/dist-git-gating-tests/check_bz:latest
  tags:
    - redhat
    - gitbz
  stage: merge_check
  script:
    - /usr/bin/check_gitbz || exit_code=$?
    - |
      if [ $exit_code -eq 77 ]; then
        echo "NOTICE: This change is being allowed without approval during the active development phase."
        exit $exit_code
      fi
    - |
      if [ $exit_code -gt 0 ]; then
        echo "ERROR: Either no related issue was found or the issues that were found were not approved for blocker/exception. Merge not allowed."
        exit $exit_code
      fi
  artifacts:
    paths:
      - "debug.log"
    when: always
    expire_in: 1 week
  rules:
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PROJECT_NAMESPACE == "osci" && $CI_COMMIT_BRANCH == "main"'

mr_comment_and_label:
  # TODO: Allow failure for now to unblock work on prototyping other jobs.
  allow_failure: true
  extends: [.with_retries]
  rules:
    # No need to sync on merge trains.
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  stage: pre_build
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/rog:latest"
  script:
    - export GITLAB_TOKEN=$(cat /opt/secrets/gitlab.token)
    - export GLAB_CONFIG_DIR=/tmp
    - >
      glab mr note -m "$MR_COMMENT" --unique -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID" || :
    - >
      if [ -z "$CI_MERGE_REQEST_LABELS" ]; then curl -X PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" --data '{"labels": "target::latest"}' https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}; fi
  needs: [check-gitbz]
  rules:
    - when: on_success

start:
  tags:
    - distrobaker
  rules:
    # Don't rebuild the image on merge trains! We want to promote the existing one
    # in that case
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  stage: start_build
  when: manual
  script:
    - export
    - echo "Starting build"

sync_mr_downstream:
  extends: [.with_artifacts, .with_retries]
  needs: [start]
  rules:
    # No need to sync on merge trains.
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  stage: build
  tags:
    - distrobaker
  script:
    - export
    - NS_COMPONENT=$(basename "$(dirname "$CI_MERGE_REQUEST_PROJECT_PATH")")/$(basename "$CI_MERGE_REQUEST_PROJECT_PATH")
    - CONTENT_LENGTH=$(curl --fail-with-body -X POST "https://distrogitsync.osci.redhat.com/$NS_COMPONENT/mr/$CI_MERGE_REQUEST_IID" -i | grep "content-length" | tr -d -c 0-9)
    # If content length is zero something has gone wrong and we need to fail.
    - |
      if [ $CONTENT_LENGTH -eq 0 ] ; then
        echo "distrogitsync responded with zero-length response. Something is wrong, failing job."
        exit 1
      fi

build_rpm:
  artifacts:
    when: always
    paths:
      - koji_build.html
    reports:
      dotenv:
        - metadata.env  # Information to pass along as needed, e.g. build ID and link
    expire_in: 6 months
    expose_as: 'Build Brew RPM log'
  extends: [.with_retries]
  needs: ["sync_mr_downstream"]
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/distrobuildsync:distrobuildsync-636b0e47"
  rules:
    # Don't rebuild the image on merge trains! We want to promote the existing one
    # in that case
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  stage: build
  script:
    # - echo "build_link=https://koji/123" >> metadata.env
    # - echo "build_id=123" >> metadata.env
    - kinit -k -t "/opt/secrets/distrobaker.keytab" "distrobaker/distrobaker.osci.redhat.com@REDHAT.COM"
    - echo "Submitting brew build!"
    # Get the build target.
    # TODO: Rewrite this, probably use rhpkg?
    - TARGET=$(curl https://gitlab.cee.redhat.com/osci/distrobaker_config/-/raw/rhel9/distrobaker.yaml|grep "build:" -A 3|grep target|grep rhel-9 -m 1|awk '{print $2}')
    - NS_COMPONENT=$(basename "$(dirname "$CI_MERGE_REQUEST_PROJECT_PATH")")/$(basename "$CI_MERGE_REQUEST_PROJECT_PATH")
    - brew build --nowait --scratch --fail-fast "$TARGET" "git+https://pkgs.devel.redhat.com/git/${NS_COMPONENT}#${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA}" | tee koji_build.log
    - echo "<html><body><h1>Brew build log</h1><pre>" > koji_build.html
    - cat koji_build.log >> koji_build.html
    - echo "</pre></body></html>" >> koji_build.html
    - TASK_ID=$(cat koji_build.log | grep "^Created task:\ " | awk '{ print $3 }')
    - echo "KOJI_TASK_ID=$TASK_ID" >> metadata.env
    - echo "KOJI_BUILD_TARGET=$TARGET" >> metadata.env
    - brew watch-task "$TASK_ID"
    # - "koji build --wait" or whatever executed on the background
    # - retrieve link to build and print
    # GitLab can get unhappy if there is no output on the console for a long while
    # - while pgrep brew ; do sleep 60; echo -ne "\0" ; done
    # - check build status and pass or fail with appropriate message
    - echo "Everything went well!"

build_centos_stream_rpm:
  extends: [.with_artifacts, .with_retries]
  needs: [start]
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/distrobuildsync:distrobuildsync-636b0e47"
  rules:
    # Don't rebuild the image on merge trains! We want to promote the existing one
    # in that case
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  stage: build
  script:
    - kinit -k -t "/opt/secrets/distrobaker.keytab" "distrobaker/distrobaker.osci.redhat.com@REDHAT.COM"
    - echo "Submitting koji scratch-build!"
    - NS_COMPONENT=$(basename "$(dirname "$CI_MERGE_REQUEST_PROJECT_PATH")")/$(basename "$CI_MERGE_REQUEST_PROJECT_PATH")
    # scoped labels are mutually exclusive so no need for if-else
    - >
      if [ "$CI_MERGE_REQUEST_LABELS" = "target::latest" ]; then koji -p stream build --fail-fast --nowait --scratch --custom-user-metadata '{"rhel-target": "latest"}' c9s-candidate "git+https://gitlab.com/redhat/centos-stream/${NS_COMPONENT}.git#${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA}" | tee koji_build.log; fi
    - >
      if [ "$CI_MERGE_REQUEST_LABELS" = "target::exception" ]; then koji -p stream build --fail-fast --nowait --scratch --custom-user-metadata '{"rhel-target": "exception"}' c9s-candidate "git+https://gitlab.com/redhat/centos-stream/${NS_COMPONENT}.git#${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA}" | tee koji_build.log; fi
    - >
      if [ "$CI_MERGE_REQUEST_LABELS" = "target::zstream" ]; then koji -p stream build --fail-fast --nowait --scratch --custom-user-metadata '{"rhel-target": "zstream"}' c9s-candidate "git+https://gitlab.com/redhat/centos-stream/${NS_COMPONENT}.git#${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA}" | tee koji_build.log; fi
    - TASK_ID=$(cat koji_build.log | grep "^Created task:\ " | awk '{ print $3 }')
    - echo "KOJI_TASK_ID=$TASK_ID" >> metadata.env
    - echo "KOJI_BUILD_TARGET=$TARGET" >> metadata.env
    - koji -p stream watch-task "$TASK_ID"
    - echo "Everything went well!"

generate_test_jobs:
  extends: [.with_retries]
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/distrobuildsync:distrobuildsync-636b0e47"
  rules:
    # No need to sync on merge trains.
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  artifacts:
    when: always
    paths:
      - test_runs.yml  # Generated dynamic pipeline
      - metadata.env  # Information to pass along as needed, e.g. build ID and link
    reports:
      dotenv:
        - metadata.env  # Information to pass along as needed, e.g. build ID and link
    expire_in: 6 months
  needs:
    - build_rpm
  stage: generate_tests
  script:
    - export RHIVOS_TOKEN=$(cat /opt/secrets/rhivos.token)
    - echo "running python script that generates test jobs based on gating setup"
    - curl -o generate-test-stage.py https://gitlab.cee.redhat.com/osci/ci-mediator/-/raw/main/scripts/generate-test-stage.py  # Creates test_runs.yaml.
    - chmod +x generate-test-stage.py
    - curl -o test_stage_template.j2 https://gitlab.cee.redhat.com/osci/ci-mediator/-/raw/main/scripts/test_stage_template.j2
    - ./generate-test-stage.py "rpms/$CI_PROJECT_NAME"

trigger_tests:
  rules:
    # No need to sync on merge trains.
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  needs:
    - job: generate_test_jobs
      artifacts: true
  stage: test
  trigger:
    include:
      - artifact: test_runs.yml
        job: generate_test_jobs
    strategy: depend
  variables:
    PARENT_PIPELINE_ID: $CI_PIPELINE_ID  # So the test pipelines know where to get metadata from

# get_all_results:
#   extends: [.with_artifacts, .with_retries]
#   needs:
#     - trigger_tests
#   stage: get_results
#   script:
#     - echo "querying osci for results"
#     - echo "printed results are here"
#     - |
#       # HELLO NASTY PROTOTYPING TO SIMULATE SOMETHING THAT CAN PASS OR FAIL ON RETRY
#       if [ $(($(date +"%M")%2)) -eq 0 ] ; then
#         echo "everything passed, congratulations"
#         wget -O test_report.xml https://gitlab.com/veruu/prototypes/-/raw/implementation/rog/dummy_results.pass.xml?ref_type=heads
#         exit 0
#       else
#         echo "a test failed, click HERE to look at logs and waive it"
#         wget -O test_report.xml https://gitlab.com/veruu/prototypes/-/raw/implementation/rog/dummy_results.fail.xml?ref_type=heads
#         exit 1
#       fi
#   allow_failure:
#     exit_codes:
#       - 111  # Let's say this exit code refers to optional tests failing

sync_downstream:
  extends: [.with_artifacts, .with_retries]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" || $CI_MERGE_REQUEST_EVENT_TYPE == "merge_train"'
  stage: build
  tags:
    - distrobaker
  script:
    - export
    - NS_COMPONENT=$(basename "$(dirname "$CI_PROJECT_PATH")")/$(basename "$CI_PROJECT_PATH")
    - curl --fail-with-body -X POST "distrogitsync.osci.redhat.com/$NS_COMPONENT/$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA"
# 
# promote_build:
#   extends: [.with_artifacts, .with_retries]
#   rules:
#     # Only promote this build when we click the merge button
#     - if: '$CI_MERGE_REQUEST_EVENT_TYPE == "merge_train"'
#   stage: promote
#   script:
#     - echo "running koji command to promote draft build to production"
